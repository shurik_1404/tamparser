<?php

namespace App\TamParserBundle\Consumers;

use App\TamParserBundle\Entity\Catalog;
use App\TamParserBundle\Repository\CatalogRepository;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Symfony\Component\DependencyInjection\ContainerInterface;

class FirstLevelParseConsumer implements ConsumerInterface
{
    /*
     * Хардкод (временно, пока не выясню, как получить этот список)
     */
    protected $firstLevelLinks = [
        'Авто' => 'https://tam.by/avto/',
        'Бытовые услуги' => 'https://tam.by/bytovye-uslugi/',
        'Государство и общество' => 'https://tam.by/obschestvo/',
        'Дети' => 'https://tam.by/deti/',
        'Домашние животные' => 'https://tam.by/dom-zhivotnye/',
        'Еда' => 'https://tam.by/eda/',
        'Красота и медицина' => 'https://tam.by/krasota/',
        'Магазины, торговые площадки' => 'https://tam.by/magaziny/',
        'Недвижимость' => 'https://tam.by/nedvizhimost/',
        'Образование' => 'https://tam.by/obrazovanie/',
        'Отдых' => 'https://tam.by/otdyh/',
        'Праздники' => 'https://tam.by/prazdniki/',
        'Спорт' => 'https://tam.by/sport/',
        'Строительство и ремонт' => 'https://tam.by/strojka/',
        'Свзяь' => 'https://tam.by/svyaz/',
        'Транспорт' => 'https://tam.by/transport/',
        'Туризм и путешествия' => 'https://tam.by/turizm/',
        'Финансы' => 'https://tam.by/finansy/',
        'В2В' => 'https://tam.by/b2b/',
    ];

    /** @var Client  */
    protected $guzzleClient;
    /** @var EntityManagerInterface  */
    protected $entityManager;
    /** @var CatalogRepository  */
    protected $catalogRepository;
    /** @var ParseFirstLevelSubCategotiesConsumer */
    protected $firstSublevelParseProducer;

    public function __construct(
        CatalogRepository $catalogRepository,
        EntityManagerInterface $entityManager,
        ContainerInterface $container
    ) {
        $this->guzzleClient = new Client();
        $this->catalogRepository = $catalogRepository;
        $this->entityManager = $entityManager;
        $this->firstSublevelParseProducer = $container->get('old_sound_rabbit_mq.parse_first_sublevel_producer');
    }

    public function execute(AMQPMessage $msg)
    {
        /** @var Catalog $catalogRootCategory */
        $catalogRootCategory = $this->catalogRepository->findOneBy(['name' => 'Компании']);

        foreach ($this->firstLevelLinks as $key => $link) {
            $catalogFirstLevelCategory = (new Catalog())
                ->setParent($catalogRootCategory)
                ->setName($key)
                ->setUrl($link)
                ->setStatus(false)
            ;

            $this->entityManager->persist($catalogFirstLevelCategory);
            $this->entityManager->flush();

            $parseData = [
                'link' => $link,
            ];

            $this->firstSublevelParseProducer->publish(serialize($parseData));
        }
    }
}