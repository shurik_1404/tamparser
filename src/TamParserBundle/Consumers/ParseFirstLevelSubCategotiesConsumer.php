<?php

namespace App\TamParserBundle\Consumers;

use App\TamParserBundle\Entity\Catalog;
use App\TamParserBundle\Repository\CatalogRepository;
use Doctrine\ORM\EntityManagerInterface;
use GuzzleHttp\Client;
use Sunra\PhpSimple\HtmlDomParser;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class ParseFirstLevelSubCategotiesConsumer implements ConsumerInterface
{
    /** @var Client  */
    protected $guzzleClient;
    /** @var CatalogRepository */
    protected $catalogRepository;
    /** @var EntityManagerInterface  */
    protected $entityManager;

    public function __construct(CatalogRepository $catalogRepository, EntityManagerInterface $entityManager)
    {
        $this->guzzleClient = new Client();
        $this->catalogRepository = $catalogRepository;
        $this->entityManager = $entityManager;
    }

    public function execute(AMQPMessage $msg)
    {
        $parseData = unserialize($msg->getBody());
        $parentCatalog = $this->catalogRepository->findOneBy(['url' => $parseData['link']]);

        $content = $this->guzzleClient->get($parseData['link']);
        $data = HtmlDomParser::str_get_html($content->getBody()->getContents());
        $firstLevelSubCategories = $data->find('dd');

        foreach ($firstLevelSubCategories as $subCategory) {
            $link = $subCategory->children(0)->getAttribute('href');
            $catalogFirstLevelSubCategory = (new Catalog())
                ->setParent($parentCatalog)
                ->setName($subCategory->plaintext)
                ->setUrl($link)
                ->setStatus(false)
            ;

            $this->entityManager->persist($catalogFirstLevelSubCategory);
            $this->entityManager->flush();

            $parseData = [
                'link' => $link,
            ];

            /////parse 2 level
        }
    }
}