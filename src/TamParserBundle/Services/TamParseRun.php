<?php

namespace App\TamParserBundle\Services;

use Symfony\Component\DependencyInjection\ContainerInterface;

class TamParseRun
{
    /** @var object|\OldSound\RabbitMqBundle\RabbitMq\Producer  */
    protected $firstLevelParseProducer;

    public function __construct(ContainerInterface $container)
    {
        $this->firstLevelParseProducer = $container->get('old_sound_rabbit_mq.parse_first_level_producer');
    }

    public function parseStart()
    {
        $this->firstLevelParseProducer->publish('');
    }
}