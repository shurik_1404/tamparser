<?php

namespace App\TamParserBundle\Commands;

use App\TamParserBundle\Services\TamParseRun;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Sunra\PhpSimple\HtmlDomParser;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TamParseCommand extends Command
{
    protected $parseRun;

    //TEST
    protected $client;

    public function __construct(TamParseRun $parseFirstTamLevel, $name = null)
    {
        $this->parseRun = $parseFirstTamLevel;
        $this->client = new Client();
        parent::__construct($name);
    }

    protected function configure()
    {
        $this->setName('app:tam_parse')
            ->setDescription('Start parse TAM');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this->testParse();

        //$this->parseRun->parseStart();
    }

    protected function testParse($uri = 'https://tam.by/avto/remont-gruz-avto/', $nextPaginationPage = 2)
    {
        ///////TEST!///////////////////////////////////////////////////////////

        try {
            $content = $this->client->get($uri);
        } catch (ClientException $exception) {
            return;
        }
        $data = HtmlDomParser::str_get_html($content->getBody()->getContents());
        $firstLevelSubCategories = $data->find('.full_link');

        foreach ($firstLevelSubCategories as $item) {
            if ($item->getAttribute('title') === false) {
                continue;
            }
            $images = $data->find('img');
            $actualImage = false;
            foreach ($images as $image) {
                if ($image->getAttribute('alt') === $item->getAttribute('title')) {
                    $actualImage = $image->getAttribute('src');
                    if ($actualImage === false || $actualImage === 'https://tam.by/images/catalog/catalog-no-photo.jpg') {
                        $actualImage = 'no logo';
                    }
                }
            }
            echo ' Title: ' . $item->getAttribute('title') .
                ' link: ' . $item->getAttribute('href') .
                ' logo: ' . $actualImage . PHP_EOL;
        }

        if ($data->find('.b-pagination-list') !== []) {
            $uriRaginatorPartPosition = stripos($uri, 'page');
            if ($uriRaginatorPartPosition !== false) {
                $uri = substr($uri, 0, $uriRaginatorPartPosition);
            }

            $this->testParse($uri . 'page' . $nextPaginationPage, $nextPaginationPage + 1);
        }


        /////////////////////END TEST/////////////////////////////////////////////
    }
}