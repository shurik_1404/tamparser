<?php

namespace App\TamParserBundle\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

class MyController
{
    public function test()
    {
        return new JsonResponse('200');
    }
}