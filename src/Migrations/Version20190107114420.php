<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190107114420 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE catalog DROP FOREIGN KEY FK_1B2C3247727ACA70');
        $this->addSql('ALTER TABLE catalog DROP FOREIGN KEY FK_1B2C3247A977936C');
        $this->addSql('CREATE TABLE companies (id INT AUTO_INCREMENT NOT NULL, catalog_id INT NOT NULL, created_at DATETIME DEFAULT NULL, updated_at DATETIME DEFAULT NULL, name VARCHAR(255) NOT NULL, url VARCHAR(255) DEFAULT NULL, alias_id INT DEFAULT NULL, status TINYINT(1) NOT NULL, INDEX IDX_8244AA3ACC3C66FC (catalog_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE catalogs (id INT AUTO_INCREMENT NOT NULL, tree_root INT DEFAULT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL, url VARCHAR(255) DEFAULT NULL, alias_id INT DEFAULT NULL, status TINYINT(1) NOT NULL, lft INT NOT NULL, rgt INT NOT NULL, lvl INT NOT NULL, INDEX IDX_F3AD370AA977936C (tree_root), INDEX IDX_F3AD370A727ACA70 (parent_id), INDEX name_idx (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE companies ADD CONSTRAINT FK_8244AA3ACC3C66FC FOREIGN KEY (catalog_id) REFERENCES catalogs (id)');
        $this->addSql('ALTER TABLE catalogs ADD CONSTRAINT FK_F3AD370AA977936C FOREIGN KEY (tree_root) REFERENCES catalogs (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE catalogs ADD CONSTRAINT FK_F3AD370A727ACA70 FOREIGN KEY (parent_id) REFERENCES catalogs (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE catalog');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE companies DROP FOREIGN KEY FK_8244AA3ACC3C66FC');
        $this->addSql('ALTER TABLE catalogs DROP FOREIGN KEY FK_F3AD370AA977936C');
        $this->addSql('ALTER TABLE catalogs DROP FOREIGN KEY FK_F3AD370A727ACA70');
        $this->addSql('CREATE TABLE catalog (id INT AUTO_INCREMENT NOT NULL, tree_root INT DEFAULT NULL, parent_id INT DEFAULT NULL, name VARCHAR(255) NOT NULL COLLATE utf8mb4_unicode_ci, url VARCHAR(255) DEFAULT NULL COLLATE utf8mb4_unicode_ci, alias_id INT DEFAULT NULL, status TINYINT(1) NOT NULL, lft INT NOT NULL, rgt INT NOT NULL, lvl INT NOT NULL, INDEX IDX_1B2C3247A977936C (tree_root), INDEX IDX_1B2C3247727ACA70 (parent_id), INDEX name_idx (name), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE catalog ADD CONSTRAINT FK_1B2C3247727ACA70 FOREIGN KEY (parent_id) REFERENCES catalog (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE catalog ADD CONSTRAINT FK_1B2C3247A977936C FOREIGN KEY (tree_root) REFERENCES catalog (id) ON DELETE CASCADE');
        $this->addSql('DROP TABLE companies');
        $this->addSql('DROP TABLE catalogs');
    }
}
